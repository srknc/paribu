export interface Currency {
  name: string;
  icon: string;
  color: string;
  decimals: number;
  min: number;
  max: number;
}

export default class CurrencyService {
  public currencies = {
    currencies: {
      tl: {
        name: 'Türk Lirası',
        icon: 'tl.png',
        color: 'ffe500',
        decimals: 2,
        min: 10,
        max: 100000
      },
      btc: {
        name: 'Bitcoin',
        icon: 'btc.png',
        color: 'FF8C00',
        decimals: 8,
        min: 0.0001,
        max: 100
      },
      usdt: {
        name: 'Tether',
        icon: 'usdt.png',
        color: '50AE94',
        decimals: 4,
        min: 0.005,
        max: 2000
      },
      eth: {
        name: 'Ethereum',
        icon: 'eth.png',
        color: '828282',
        decimals: 3,
        min: 0.25,
        max: 500000
      },
      xrp: {
        name: 'Ripple XRP',
        icon: 'xrp.png',
        color: '0080BE',
        decimals: 0,
        min: 1000,
        max: 1000000
      },
      trx: {
        name: 'Tron',
        icon: 'trx.png',
        color: 'C23631',
        decimals: 0,
        min: 5000,
        max: 5000000
      }
    },
    success: true,
    message: 'OK'
  };

  public dummyHttpCall(): Promise<any> {
    return new Promise((resolve, reject) => {
      window.setTimeout(() => resolve(this.currencies), 400);
    });
  }
}
